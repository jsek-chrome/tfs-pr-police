# TFS - PR Police

Chrome extension that makes it possible to create custom policies for Pull Requests on TFS. Take enforcing quality contribution to the next level 🚀.

---

![](screenshots/screenshot_mine_1.png)

---

## TODO

- [x] Split `content.coffee` into Components and Services
- [x] Add `.catch`
- [ ] Add missing functionality (counters, popup)
- [x] Replace content of "Pull Requests" tab instead of adding new
- [ ] Consider expand/collapse to see basic details about the PR
  - [ ] Files list
  - [x] Commits (compact view)
  - [ ] Quality metrics
  - [ ] Allow accepting/rejecting

## Future enhancements of Code Review process

- Commits list (compact) visible above the discussion
- Quality? checklist for author of the changes
- Popup before merging/approving changes
  - Force accepting (consciously) commits that failed policy validation (one by one)
  - Force accepting (consciously) missing tests if were expected

---

## Setup

```bash
npm install
```

### Build

```bash
npm run build
```

### Pack before publish

1. Generate `key.pem` once using `npm run keygen` command

Create `*.crx` and `*.zip` packages

```bash
gulp pack
```

## Credits

Icons made by [Freepik](http://www.freepik.com) from [Flaticon](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)