getFromStore = (type) ->
    new Promise (resolve) ->
        try
            chrome.runtime.sendMessage {type}
            , resolve
        catch err
            console.error(err)

String::normalizedUrl = ->
    return unless @length
    httpCheck = /^http(s)?:\/\//i
    url = if httpCheck.test(@) then @ else "http://#{@}"
    url.replace(/\/$/,'')

createLink = (selector, url) ->
    $(selector).click ->
        chrome.tabs.create
            url: url
            active: true
        window.close()

jsonFetchOptions = 
    credentials: 'include'
    headers:
        'Access-Control-Allow-Origin': '*'
        'Accept' : 'application/json'

fetchJson = (url) ->
    response = await fetch url, jsonFetchOptions
    await response.json()

notConfigured = ->
    $('.popup.configured').hide()
    $('.popup.not-configured').show()
    createLink '.open-options', "options/options.html"

notLoggedIn = (tfsUrl) ->
    $('.popup.configured').hide()
    $('.popup.not-logged-in').show()
    createLink '.open-tfs', "#{tfsUrl}/_projects"

$ -> do ->
    { settings } = await getFromStore 'OPTIONS_GET'
    tfsUrl = settings?.model?.TFS?.URL?.normalizedUrl()

    return notConfigured() unless tfsUrl

    try
        { identity: { TeamFoundationId: UserId } } = await fetchJson "#{tfsUrl}/_api/_common/GetUserProfile"
    catch err
        return notLoggedIn(tfsUrl)

    createLink '.open-active-pr', "#{tfsUrl}/_pulls"

    [
        { count: created }
        { count: assigned }
    ] = await Promise.all([
            "status=Active&creatorId=#{UserId}"
            "status=Active&reviewerId=#{UserId}"
        ]
        .map (searchCriteria) -> fetchJson "#{tfsUrl}/_apis/git/PullRequests?#{searchCriteria}"
    )

    $('.created-pr-counter').text created
    $('.assigned-pr-counter').text assigned