# chrome.commands.onCommand.addListener (command) ->
#   switch command
#     when 'copy-to-email' then
#       chrome.tabs.executeScript null, file: 'scripts/copy-to-email.js'
#     else
#       console.log 'Unrecognized command'

# storage = new Storage()

handlers = []

setupHandler = (typePrefix, property) ->
  handlers.push
    type: typePrefix + '_SET'
    action: ({data}) ->
      new Promise (resolve, reject) ->
        obj = {}
        obj[property] = data
        chrome.storage.sync.set obj, resolve

  handlers.push
    type: typePrefix + '_GET'
    action: ->
      new Promise (resolve, reject) ->
        chrome.storage.sync.get property, resolve

setupHandler 'OPTIONS', 'settings'
setupHandler 'CACHE', 'store'

chrome.runtime.onMessage.addListener (message, sender, sendResponse) ->
  {action} = handlers.filter(({type}) -> type is message.type)[0] or {}
  if action?
    action(message).then (result) ->
      sendResponse Object.assign result or {}, isSuccess: true
  else if message.type is 'OPTIONS_OPEN'
    chrome.tabs.create
      url: 'options/options.html'
      active: true
    sendResponse {isSuccess: false, error}
  else
    error = "Unrecognized message type [#{message.type}]"
    sendResponse {isSuccess: false, error}
    console.log error
  return true