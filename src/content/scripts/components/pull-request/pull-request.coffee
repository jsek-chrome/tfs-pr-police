class PullRequest
    constructor: ({@url, @repoName, @title, @id, @created, @from, @to, @createdBy, @conflicts, @reviewers = [], commentsLoaded, @commitsUrl, @commitUrlBase}) ->
        @container = $('<tr class="vc-pullrequest-results-row allowed"></tr>')
        commentsLoaded
        .then (data) =>
            @commentsCount = _computeNumberOfPRCommentsInThread data
            @update()
        .catch console.error

    setEvents: ->
        @container
        .off 'click'
        .on 'click', '.vs-pullrequest-expand-details', (e) =>
            e.preventDefault()
            Utils.ajax @commitsUrl
            .then (response) =>
                $(e.currentTarget).find('.bowtie-icon').toggleClass 'bowtie-chevron-right bowtie-chevron-down'

                if @container.next().hasClass('vc-pullrequest-details')
                    @container.next().remove()
                else
                    commits = response.value.map (c) =>
                        c.url = @commitUrlBase + c.commitId
                        return c
                    @container.after TPL['pullrequest-details-row'] model: {commits}

            .catch console.error

    _computeNumberOfPRCommentsInThread = (data) ->
        # copied from TFS sources
        sum = 0
        for k,v of data?.value
            sum += v.filter((x) ->
                    x.properties and (if x.properties.CodeReviewThreadType then false else x.commentsCount isnt 0)
                ).length
        return sum

    update: ->
        @container.html TPL['pullrequest-results-row'](model:@)

    getContainer: ->
        @update()
        @container