class MinePullRequests
    constructor: ({@container}) ->

    instrumentGroupElement: ({element, containerClass, counterClass}) ->
        element.show()
            .addClass('ext') # mark DOM element to prevent loading in the loop
        element.find('tbody')
            .addClass containerClass
        element.find('.vc-pullrequest-entry-col-primary > .vc-pullrequest-subtle-text')
            .addClass counterClass
            .removeAttr 'data-bind'
            .show()

    inject: ({loadRequested, loadAssigned, loadAssignedToTeam}) ->

        # hide dummy info

        $info = @container.find('.vc-pullrequest-information')
        if $info.length > 0 and /No pull requests matched the filter criteria/i.test $info.text()
            $info.parent().remove()

        # instrument DOM

        $groups = @container.find('.vc-pullrequest-results-table')
            .toArray()
            .map $

        $groupForRequestedByMe = $groups.filter (g) -> /(Requested|Created) by me/i.test g.find('thead td:first-child').text()
        if $groupForRequestedByMe.length > 0
            @instrumentGroupElement
                element: $groupForRequestedByMe[0]
                containerClass: 'all-pr--requested__container'
                counterClass: 'all-pr--requested__counter'

        $groupForAssignedToMe = $groups.filter (g) -> /Assigned to me/i.test g.find('thead td:first-child').text()
        if $groupForAssignedToMe.length > 0
            @instrumentGroupElement
                element: $groupForAssignedToMe[0]
                containerClass: 'all-pr--assigned__container'
                counterClass: 'all-pr--assigned__counter'

        $groupForAssignedToMe = $groups.filter (g) -> /Assigned to team/i.test g.find('thead td:first-child').text()
        if $groupForAssignedToMe.length > 0
            @instrumentGroupElement
                element: $groupForAssignedToMe[0]
                containerClass: 'all-pr--team__container'
                counterClass: 'all-pr--team__counter'

        # load and insert all pull requests

        loadRequested
        .then (pullRequests) ->
            $('.all-pr--requested__counter').text " (#{pullRequests.length})"
            $('.all-pr--requested__container')
            .html pullRequests.map((pr) -> pr.getContainer()).reduce ((acc, next) -> acc.add next), $()
            pullRequests.forEach (pr) -> pr.setEvents()
        .catch console.error

        loadAssigned
        .then (pullRequests) ->
            $('.all-pr--assigned__counter').text " (#{pullRequests.length})"
            $('.all-pr--assigned__container')
            .html pullRequests.map((pr) -> pr.getContainer()).reduce ((acc, next) -> acc.add next), $()
            pullRequests.forEach (pr) -> pr.setEvents()
        .catch console.error

        loadAssignedToTeam
        .then (pullRequests) ->
            $('.all-pr--team__counter').text " (#{pullRequests.length})"
            $('.all-pr--team__container')
            .html pullRequests.map((pr) -> pr.getContainer()).reduce ((acc, next) -> acc.add next), $()
            pullRequests.forEach (pr) -> pr.setEvents()
        .catch console.error