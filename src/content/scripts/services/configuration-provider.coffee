Array::toObject = (keySelect, valueProperty) ->
    if typeof(keySelect) is 'function'
        @reduce ((acc, next) -> acc[keySelect next] = next[valueProperty]; acc), {}
    else
        @reduce ((acc, next) -> acc[next[keySelect]] = next[valueProperty]; acc), {}

class ConfigurationProvider

    constructor: ->
        @loaded = new Promise @init

    init: (resolve, reject) ->
        try
            settings = window.settings
            tfsBaseUrl = settings?.model?.TFS?.URL?.normalizedUrl()
            teamCityBaseUrl = settings?.model?.TeamCity?.URL?.normalizedUrl()

            resolve
                baseUrl: tfsBaseUrl
                teamCityBaseUrl: teamCityBaseUrl
                buildTypes: settings?.model?.TeamCity?.BuildConfigurationsMapping.toObject(((m) -> m.Repository.name), 'BuildType')
                toggles: settings?.toggles.toObject('Id', 'Enabled')
                policies: settings?.policies.toObject('Id', 'Enabled')
        catch err
            reject err
