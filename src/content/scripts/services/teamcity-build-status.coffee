class TeamCityBuildStatus

    _disableMerge = ->
        $('button:contains(Complete pull request)')
        .replaceWith('<button class="add-panel-button btn-cta" disabled="disabled" title="Disabled to prevent build failure">Complete pull request</button>')

    constructor: ({ @container, @config, @repoName, @branch }) ->

        unless @branch
            console.error 'Branch name is missing, so I cannot pull build status for this PR'
            return

        @Inject()

        @buildType = @config.buildTypes[@repoName]
        unless @buildType
            @RefreshStatus configured: false
            $('.ext-open-options').click (e) ->
                e.preventDefault()
                chrome.runtime.sendMessage {type: 'OPTIONS_OPEN'}, ->

        @baseUrl = @config.teamCityBaseUrl
        @tcConnector = new TeamCityConnector(@baseUrl, @buildType, @branch)

        @isActive = $('.status-indicator').hasClass('active')

    Inject: ->
        @container.append TPL['build-status-policy'] model: {}

    Init: ->
        if @isActive
            repeat = =>
                console.log('REPEAT')
                status = await @ReloadStatus()
                @RefreshStatus(status)
                setTimeout repeat, 2000
            repeat()
        else
            @RefreshStatus await @ReloadStatus()
                # (?) Handle long-living branches? (by correlating dates)

    ReloadStatus: ->

        {count, info, url} = await @tcConnector.FetchBuildStatus()

        policySatisfied = (not info?) or info.status is 'SUCCESS'

        if @config.policies['TeamCity-blocks-pull-request'] and @isActive

            if policySatisfied and @isMergeDisabled
                @ToggleMergeButton enabled: true

            if not policySatisfied and not @isMergeDisabled
                @ToggleMergeButton enabled: false

        return {
            configured: true
            active: @isActive
            count: count
            info: info
            url: do =>
                switch
                    when info?.state in ['queued', 'running']                   then "#{@baseUrl}/viewLog.html?buildTypeId=#{@buildType.id}&buildId=#{info.id}"
                    when info?.state is 'finished' and info.status is 'SUCCESS' then url
                    when info?.state is 'finished' and info.status is 'FAILURE' then "#{@baseUrl}/viewLog.html?buildTypeId=#{@buildType.id}&buildId=#{info.id}&tab=buildLog&logTab=tail"
                    else url + '&tab=buildTypeStatusDiv'
        }

    RefreshStatus: (status) ->
        @container
        .find('.teamcity-build-custom-policy')
        .replaceWith TPL['build-status-policy'] model: status

    ToggleMergeButton: ({enabled}) ->
        @isMergeDisabled = not enabled
        if enabled
            $('.disabled-merge-button')
            .removeClass('disabled-merge-button')
            .removeAttr('disabled')
            .attr('title', '')

        else
            $('button:contains(Complete)')
            .addClass('disabled-merge-button')
            .attr('disabled', 'disabled')
            .attr('title', 'Disabled to prevent build failure')
            .off()