class CommitMessageFormatter

    getCommitMessageModel: (msg) ->
        text: msg
        chunks: _splitCommitMessage _getSubject msg
        hasBody: msg.length > 80 or msg.split('\n\n').length > 1
        bodyHTML: _getBody(msg).replace(/\n/g, '<br>')
    
    _linkablePattern = /(US[0-9]+|DE[0-9]+|TA[0-9]+)/
    _linkBase = 'https://rally1.rallydev.com/slm/rally.sp?#/search?keywords='

    _getSubject = (msg) ->
        return msg.split('\n\n')[0][0...80].replace(/\n/g, '')

    _getBody = (msg) ->
        parts = msg.split('\n\n')
        subjectRest = parts[0][80..]
        return subjectRest.concat parts[1..].join('\n\n')

        # full commit body: /tfs/{collection}/{project}/_apis/git/repositories/{repository}/commits/{id}        

    _splitCommitMessage = (msg) ->
        msg
        .split _linkablePattern
        .map (text) ->
            isLink = _linkablePattern.test text
            return {
                text
                isLink
                url: if isLink then _linkBase + text else null
            }