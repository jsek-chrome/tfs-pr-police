
class AllPullRequests
        
    constructor: ({@_cache = {}, @userId, @teamId, @baseUrl}) ->
        @collectionUrl = @baseUrl.substr 0, @baseUrl.lastIndexOf('/')

    getRepositories: ->
        new Promise (resolve, reject) =>
            unless @_cache.repos
                @loadRepositories()
                .then (repos) =>
                    @_cache.repos = repos
                    resolve(@_cache.repos)
                .catch reject
            else
                resolve(@_cache.repos)

    loadRepositories: ->
        Utils.ajax "#{@baseUrl}/_apis/git/repositories"

    loadAssignedToMe: (repos, projectId) ->
        @loadPullRequests repos, projectId, "reviewerId=#{@userId}"

    loadAssignedToTeam: (repos, projectId) ->
        @loadPullRequests repos, projectId, "reviewerId=#{@teamId}"

    loadRequestedByMe: (repos, projectId) ->
        @loadPullRequests repos, projectId, "creatorId=#{@userId}"

    loadPullRequests: (repos, projectId, userParam) ->
        new Promise (resolve, reject) =>
            requests = repos.value
            .map (r, i) => @_loadPRList r.id, r.name, projectId, userParam
            
            Promise.all requests
            .then (lists) ->
                resolve lists.reduce ((acc,next) -> acc.concat(next)), []
            .catch (err) ->
                console.error err
                reject err

    _loadPRList: (id, repoName, projectId, userParam) ->
        Utils.ajax("#{@baseUrl}/_apis/git/repositories/#{id}/pullRequests?status=active&#{userParam}")
        .then (prs) =>
            prs.value.map (pr) =>
                return new PullRequest {
                    url: "#{@baseUrl}/_git/#{repoName}/pullrequest/#{pr.pullRequestId}"
                    repoName: repoName
                    title: pr.title
                    id: pr.pullRequestId
                    created: (new Date(pr.creationDate)).toLocaleDateString()
                    from: do =>
                        title = pr.sourceRefName.replace('refs/heads/', '')
                        url = "#{@collectionUrl}/#{repoName}#version=GB#{encodeURIComponent(title)}"
                        { title, url }
                    to: do =>
                        title = pr.targetRefName.replace('refs/heads/', '')
                        url = "#{@collectionUrl}/#{repoName}#version=GB#{encodeURIComponent(title)}"
                        { title, url }
                    createdBy:
                        displayName: pr.createdBy.displayName
                        id: pr.createdBy.id
                        imageUrl: Utils.relativeImageUrl pr.createdBy.imageUrl
                    conflicts:
                        pr.mergeStatus is 'conflicts'
                    reviewers: pr.reviewers?.map (r) ->
                        return {
                            displayName: r.displayName
                            imageUrl: Utils.relativeImageUrl r.imageUrl
                            vote: r.vote
                        }
                    commentsLoaded: Utils.ajax
                        url: "#{@collectionUrl}/_apis/discussion/threadsBatch"
                        method: 'POST'
                        contentType: 'application/json'
                        headers: Accept: 'application/json;api-version=3.0-preview.1'
                        data: "[\"vstfs:///CodeReview/CodeReviewId/#{projectId}%2F#{pr.pullRequestId}\"]"
                    commitsUrl: "#{@baseUrl}/_apis/git/repositories/#{id}/pullRequests/#{pr.pullRequestId}/commits"
                    commitUrlBase: "#{@collectionUrl}/#{repoName}/commit/"
                }
        # .catch on Promise.all
    
    sync: () ->