class PullRequestsListBootstrapper

    selectors =
        tabsMenu:
            legacy : '.hubs'
            current : '.hub-list > .hubs-menubar'

    _isOnPullRequestsTab = ->
        /\/pullrequests/.test(location.href)

    _isOnMineTab = ->
        $('a[href="#_a=mine"]').parent().hasClass('selected')

    @start: ->
        return unless _isOnPullRequestsTab() and _isOnMineTab()

        configurationProvider = new ConfigurationProvider()
        config = await configurationProvider.loaded
        return unless config.toggles['All-my-pull-requests']

        $target = $([
            selectors.tabsMenu.legacy
            selectors.tabsMenu.current
        ].join(', '))

        if $target.length is 0
            setTimeout(PullRequestsListBootstrapper.start, 500)
            return

        _loadMissingPullRequests()
        setTimeout ->
            Utils.onElementContentChange {target: $('.hub-pivot-content')[0] }, _loadMissingPullRequests
        , 10

    _loadMissingPullRequests = ->
        return unless _isOnMineTab()

        $container = $('.navigation-view-tab:visible')
        $updatedGroups = $container.find('.vc-pullrequest-results-table.ext')
        return if $updatedGroups.length > 0
        
        minePullRequests = new MinePullRequests(container: $container)

        PullRequestsListBootstrapper.pullRequestsLoaded or= new Promise _loadPullRequests
        PullRequestsListBootstrapper.pullRequestsLoaded
        .then (res) ->
            minePullRequests.inject(res)
            _hideInvitationMessage()
        .catch console.error
        
    _loadPullRequests = (resolve, reject) ->
        webPageData = new WebPageData()

        configurationProvider = new ConfigurationProvider()
        config = await configurationProvider.loaded
        
        {userId, teamId, projectId} = await webPageData.loaded

        allPullRequests = new AllPullRequests
            baseUrl: config.baseUrl
            userId: userId
            teamId: teamId
    
        allPullRequests.getRepositories()
        .then (repos) ->
            resolve
                loadRequested: allPullRequests.loadRequestedByMe(repos, projectId)
                loadAssigned: allPullRequests.loadAssignedToMe(repos, projectId)
                loadAssignedToTeam: allPullRequests.loadAssignedToTeam(repos, projectId)

        .catch reject
        
    _hideInvitationMessage = ->
        $('.empty-pr-list-container').hide()