class Utils
    @debounce: (func, threshold, execAsap) ->
        timeout = null
        (args...) ->
            obj = this
            delayed = ->
                func.apply(obj, args) unless execAsap
                timeout = null
            if timeout
                clearTimeout(timeout)
            else if (execAsap)
                func.apply(obj, args)
            timeout = setTimeout delayed, threshold || 100

    @waitForElement: ({ selector, target = document.body }) ->
        new Promise (resolve, reject) ->
            timeout = setTimeout ->
                reject(message: "Timeout 20s occured while waiting for element [#{selector}]")
            , 20 * 1000

            observer = new MutationObserver Utils.debounce (mutations) ->
                $expectedElement = $(selector)
                if $expectedElement.length > 0
                    observer.disconnect()
                    clearTimeout timeout
                    resolve $expectedElement

            observer.observe target,
                attributes: false
                characterData: false
                childList: true
                subtree: true

    @onElementContentChange: ({target = document.body}, callback) ->
        observer = new MutationObserver Utils.debounce (mutations) ->
            callback()

        observer.observe target,
            attributes: false
            characterData: false
            childList: true
            subtree: true

    @relativeImageUrl: (rawImageUrl) ->
        @relativeUrl(rawImageUrl) + '&__v=5'

    @relativeUrl: (rawUrl) ->
        rawUrl.slice(rawUrl.indexOf('/tfs/'))

    @relativeDate: $.timeago

    _jsonFetchOptions = 
        credentials: 'include'
        headers:
            'Access-Control-Allow-Origin': '*'
            'Accept' : 'application/json'

    @fetch:
        json: (url) ->
            response = await fetch url, _jsonFetchOptions
            await response.json()

    @ajax: (args...) ->
        new Promise (resolve, reject) ->
            $.ajax.apply($, args)
            .then resolve
            .fail reject