
class TeamCityConnector

    constructor: (@teamcityAddress, @buildType, @branch) ->

    FetchBuildStatus: ->
        url = "#{@teamcityAddress}/guestAuth/app/rest/builds/?locator=buildType:#{@buildType.id},branch:(name:#{@branch}),state:any"

        {build, count, href} = await Utils.fetch.json url

        return
            count: count
            info: build?[0]
            url: "#{@teamcityAddress}/viewType.html?buildTypeId=#{@buildType.id}&tab=buildTypeHistoryList&branch=#{encodeURIComponent(@branch)}"