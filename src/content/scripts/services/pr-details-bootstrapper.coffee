class PullRequestDetailsBootstrapper

    _initialized = false

    patterns =
        prPage: /\/[^\/]+\/pullrequest\/[0-9]+/
        repoFromUrl: /\/([^\/]+)\/pullrequest\/[0-9]+/
        prIdFromUrl: /\/pullrequest\/([0-9]+)/

    selectors =
        historyContainer: '.vc-history-list'
        detailsPanel: '.vc-pullrequest-view-details-panel, .vc-pullrequest-review-view'
        templateRoot: '.vs-commits-compact-list'
        leftSections: '.leftPane-static.with-background .vc-pullrequest-leftpane-section'
        policiesSection: '.policies-section'
        branchName: '.vc-pullrequest-detail-branch-name'

    _isOnPullRequestPage = ->
        patterns.prPage.test(location.href)

    _isCodePreviewPage = ->
        /view=(content|compare)/.test(location.href)

    _setNavigationHandler = ->
        Utils.onElementContentChange {target: $('.vc-pullrequest-details-fixed')[0]}, ->
            $(selectors.templateRoot).toggleClass 'hidden', !_shouldBeVisible()
            
    _shouldBeVisible = () -> _isOnPullRequestPage() and !_isCodePreviewPage()

    @start: ->

        unless _isOnPullRequestPage()
            return

        _setNavigationHandler()

        webPageData = new WebPageData()
        configurationProvider = new ConfigurationProvider()
        messageFormatter = new CommitMessageFormatter()
        
        repoName = location.href.match(patterns.repoFromUrl)[1]
        config = await configurationProvider.loaded 
        
        do ->
            return unless config.toggles['Commit-compact-list'] and config.teamCityBaseUrl

            Promise.all([
                webPageData.loaded
                Utils.waitForElement {selector: selectors.detailsPanel}
            ])
            .then ([{userId, teamId, repositoryId}, $detailsPanel]) ->

                pullRequestId = location.href.match(patterns.prIdFromUrl)[1]
                commitsUrl = "#{config.baseUrl}/_apis/git/repositories/#{repositoryId}/pullRequests/#{pullRequestId}/commits"
                commitUrlBase = "#{config.baseUrl}/_git/#{repoName}/commit/"

                $detailsPanel
                .off 'click'
                .on 'click', '.vs-commit-message-expander', (e) ->
                    $(e.currentTarget)
                    .hide()
                    .next()
                    .removeClass('hidden')

                response = await Utils.ajax commitsUrl
                
                setTimeout (->
                    $detailsPanel
                    .prepend do ->
                        commits = response.value.map (c) =>
                            c.url = commitUrlBase + c.commitId
                            c.message = messageFormatter.getCommitMessageModel c.comment
                            return c
                        TPL['commit-compact-list'] model: {commits, visible:_shouldBeVisible()}

                    shiftFixedElements = $(selectors.templateRoot).outerHeight()
                    console.debug($('.hub-content').css('top', (_,t) -> "calc(#{t} + #{shiftFixedElements}px)"))
                    return
                    ), 100

            .catch console.error

        AddPolicyStatus = ->
            return unless config.toggles['TeamCity-builds'] and config.teamCityBaseUrl

            Promise.all([
                Utils.waitForElement {selector: selectors.leftSections}
                Utils.waitForElement {selector: selectors.branchName}
            ])
            .then ([$leftSections, $branchName]) ->

                $policiesSection = $leftSections.find(selectors.policiesSection)
                if $policiesSection.length is 0
                    $leftSections
                    .first()
                    .after '
                    <div class="vc-pullrequest-leftpane-section">
                        <div class="vc-pullrequest-leftpane-section-title">
                            <span>Policies</span>
                        </div>
                        <div class="divider"></div>
                        <div class="policies-section"></div>
                    </div>
                    '
                    $policiesSection = $(selectors.policiesSection)
                

                branch = $branchName.first().text()
                new TeamCityBuildStatus({ container: $policiesSection, config, repoName, branch })
                .Init()

            .catch (err) ->
                console.warn err
                setTimeout AddPolicyStatus, 2000
        
        AddPolicyStatus()