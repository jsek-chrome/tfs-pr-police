# #####################################
# WebPage Data Provider
#

class WebPageData

    constructor: ->
        @loaded = new Promise @init

    init: (resolve, reject) ->
        try
            setTimeout =>
                webPageDataScriptContent = $('script')
                    .filter((_,x) -> /"user":{"id":"([0-9a-z\-]+)/.test x.innerText)[0]
                    ?.innerText
                prOptionsContent = $('.options,.vss-web-page-data')
                    .filter((_,x) -> /"gitRepository":{"id":"([0-9a-z\-]+)/.test x.innerText)[0]
                    ?.innerText
                
                resolve
                    userId: webPageDataScriptContent.match(/"user":{"id":"([0-9a-z\-]+)/)[1]
                    teamId: webPageDataScriptContent.match(/"team":{"id":"([0-9a-z\-]+)/)?[1]
                    projectId: webPageDataScriptContent.match(/"project":{"id":"([0-9a-z\-]+)/)[1]
                    repositoryId: prOptionsContent?.match(/"gitRepository":{"id":"([0-9a-z\-]+)/)[1]

            , 10
        catch
            message 'User ID or Project ID not found. New way of getting WebPageData should be implemented.'
            console.warn message
            reject error: message