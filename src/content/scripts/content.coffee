getOptions = () ->
    new Promise (resolve) ->
        try
            chrome.runtime.sendMessage type: 'OPTIONS_GET'
            , resolve
        catch err
            console.error(err)

String::normalizedUrl = ->
    return unless @length
    httpCheck = /^http(s)?:\/\//i
    url = if httpCheck.test(@) then @ else "http://#{@}"
    url.replace(/\/$/,'')

$ -> do ->
    { settings } = await getOptions()

    tfsUrl = settings?.model?.TFS?.URL?.normalizedUrl().toLowerCase()
    return unless location.href.toLowerCase().indexOf(tfsUrl) is 0

    window.tfsUrl = tfsUrl
    window.settings = settings

    console.debug 'This page matches ' + tfsUrl

    PullRequestsListBootstrapper.start()
    PullRequestDetailsBootstrapper.start()
    