Function::traced = -> () =>
    result = do @
    console.debug result
    return result

String::normalizedUrl = ->
    return unless @length
    httpCheck = /^http(s)?:\/\//i
    url = if httpCheck.test(@) then @ else "http://#{@}"
    url.replace(/\/$/,'')

String::startsWith = (prefix) ->
    @indexOf(prefix) is 0

Array::remove = (e) ->
    @splice(@indexOf(e), 1)

DefaultExperimentalFeatures = [
    { Id: 'TeamCity-builds', Title: 'TeamCity build status policy', Available: true, Enabled: false }
    { Id: 'All-my-pull-requests', Title: 'Display my pull requests across all projects', Available: true, Enabled: false }
    { Id: 'Commit-compact-list', Title: 'Compact commit list above Pull Request details', Available: true, Enabled: false }
    { Id: 'CodeFreeze', Title: 'Code Freeze support', Available: false, Enabled: false } # (?) Could it be fetched from TeamCity
    { Id: 'Notifications', Title: 'Notifications about Pull Requests', Available: false, Enabled: false }
    { Id: 'Quality-metrics', Title: 'Validation, Quality metrics & KPIs (maybe someday)', Available: false, Enabled: false }
]

DefaultPolicies = [
    {
        Id: 'TeamCity-blocks-pull-request'
        Title: 'TeamCity build must pass', 
        Description: 'Assuming TeamCity is reachable, PR page will pull build run status and lock "Complete" button until all tests pass',
        Available: true, # 
        Enabled: false
    }
    {   
        Title: 'Strict commit messages',
        Description: 'Disallow commit messages that does not explain purpose (to short, copied from US/DE, regex - common antipatterns, warn - missing US/DE number, warn - invalid merges',
        Available: false,
        Enabled: false
    }
    {
        Title: 'Minimal number of reviewers, disallow self-approve',
        Description: 'Enforce strict collaboration rules',
        Available: false,
        Enabled: false
    }
    {
        Title: 'Block when title prefixed with `WIP:`',
        Description: 'Prevent merging pull requests created for early feedback',
        Available: false,
        Enabled: false
    }
]

class OptionsPage
    constructor: ($timeout, $element, $scope, fetch, CacheService, history) ->
        @scope = $scope
        @element = $element
        @timeout = $timeout
        @fetch = fetch
        @Cache = CacheService
        @history = history

        @getCachedRepositories = @getCachedRepositories.bind(@)
        @getCachedBuildTypes = @getCachedBuildTypes.bind(@)
        @Hints = {}
        @Status = {}
        @Model = {}

        @$onInit = ->
            $('.collapsible').collapsible()
            $timeout =>
                @Loaded = true
            , 500

        chrome.runtime.sendMessage (type: 'OPTIONS_GET'), (result) =>
            if result?.isSuccess
                {
                    model: @Model
                    toggles: @ExperimentalFeatures
                    policies: @Policies
                } = result.settings or {}

            @Model or= {}

            unless @Model.TFS?.URL
                return @scanToGetHints
                    keyword: ':8080/tfs'
                    regex: /https?:\/\/(.*:8080\/tfs\/[_a-z]+)\//i
                    setHintValue: (v) => @Hints.TFS = v

            unless @Model.TeamCity?.URL
                @scanToGetHints
                    keyword: 'viewType.html?buildTypeId='
                    regex: /https?:\/\/(.*)\/viewType.html\?buildTypeId=/i
                    setHintValue: (v) => @Hints.TeamCity = v

            @FullFormVisible = true

            @ExperimentalFeatures ?= DefaultExperimentalFeatures
            @Toggles =
                TeamCity: @ExperimentalFeatures[0]
            
            @Policies ?= DefaultPolicies

            @updatePoliciesVisibility()
            $scope.$watch '$ctrl.Toggles.TeamCity', (=> @updatePoliciesVisibility()), true
            
            @onTfsUrlUpdated().catch console.warn
            @onTeamCityUrlUpdated().catch console.warn

            @timeout =>
                @element.find('label + input').trigger('change')
            , 500

    scanToGetHints: ({keyword, regex, setHintValue}) ->
        {url} = await @history.MostPopularLast7Days match: keyword
        urlToCheck = url.match(regex)?[0]
        if urlToCheck and await @fetch.check "#{urlToCheck.normalizedUrl()}/_apis/git/repositories"
             setHintValue urlToCheck.match(regex)?[1]

    onTfsHintClick: ->
        @Model.TFS or= {}
        @Model.TFS.URL = @Hints.TFS
        @timeout =>
            @element.find('label + input').trigger('change')
            @timeout (=> @Save()), 200
        , 1

    onTeamCityHintClick: ->
        @Model.TeamCity.URL = @Hints.TeamCity
        @timeout =>
            @element.find('label + input').trigger('change')
            @onTeamCityUrlUpdated()
        , 1

    onTfsUrlUpdated: ->
        @Status.TFS =
            Checking: true

        if @Model.TFS.URL
            response = await @fetch.json "#{@Model.TFS.URL.normalizedUrl()}/_apis/git/repositories"
            @Status.TFS =
                Checking: false
                Reachable: true
                RepositoriesCount: response.count
            @Cache.OrderUpdate 'Repositories', url: @Model.TFS.URL.normalizedUrl()
            @scope.$apply()

    getCachedRepositories: -> @Cache.Store.Repositories
    getCachedBuildTypes: -> @Cache.Store.BuildTypes

    updatePoliciesVisibility: ->
        @Policies
            .filter (p) -> p.Id?.startsWith 'TeamCity-'
            .forEach (p) => p.Available = @Toggles.TeamCity.Enabled

    onTeamCityUrlUpdated: ->
        @Status.TeamCity =
            Checking: @Toggles?.TeamCity.Enabled
            
        if @Toggles?.TeamCity.Enabled and @Model.TeamCity.URL
            {version} = await @fetch.json "#{@Model.TeamCity.URL.normalizedUrl()}/app/rest/server"
            @Status.TeamCity =
                Checking: false
                Reachable: true
                Version: version
            @Cache.OrderUpdate 'BuildTypes', url: @Model.TeamCity.URL.normalizedUrl()
            @scope.$apply()

    AddBuildConfigurationsMapping: ($event) ->
        $event.preventDefault()
        @Model.TeamCity.BuildConfigurationsMapping or= []
        @Model.TeamCity.BuildConfigurationsMapping.push {}

    RemoveBuildConfigurationsMapping: ($event, mapping) ->
        $event.preventDefault()
        @Model.TeamCity.BuildConfigurationsMapping.remove mapping

    Save: ->
        chrome.runtime.sendMessage type: 'OPTIONS_SET', data: {
            model: @Model
            toggles: @ExperimentalFeatures
            policies: @Policies
        }, (result) =>
            if result?.isSuccess
                Materialize.toast 'Saved', 4000
                unless @FullFormVisible
                    setTimeout (-> location.reload()), 1000
            else
                Materialize.toast '
                    <div>
                        <span style="color:#f44336">Argh... Error while saving settings</span>
                        <br/>
                        <small>Please, check console for details</small>
                    </div>
                ', 5000
                if result?.error then console.error result.error

angular.module('options')
.component 'optionsPage',
    bindings:
        count: '='
    controller: OptionsPage
    template: TPL['options-page.component']()

    
        