class ExperimentalFeatures
    constructor: ->

angular.module('options')
.component 'experimentalFeatures',
    bindings:
        ngModel: '='
    controller: ExperimentalFeatures
    template: TPL['experimental-features.component']()