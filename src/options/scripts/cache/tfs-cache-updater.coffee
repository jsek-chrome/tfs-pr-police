class TfsCacheUpdater
    constructor: (CacheService, fetch) ->
        @fetch = fetch
        CacheService.RegisterUpdater 'Repositories', @Update.bind @

    Update: (args) ->
        if args.url then @baseUrl = args.url
        {value: repositories} = await @fetch.json "#{@baseUrl}/_apis/git/repositories"
        return repositories

angular.module('cache')
.service 'TfsCacheUpdater', TfsCacheUpdater