class TeamCityCacheUpdater
    constructor: (CacheService, fetch) ->
        @fetch = fetch
        CacheService.RegisterUpdater 'BuildTypes', @Update.bind @

    Update: (args) ->
        if args.url then @baseUrl = args.url
        {buildType: configurations} = await @fetch.json "#{@baseUrl}/app/rest/buildTypes/?locator=paused:false"
        return configurations

angular.module('cache')
.service 'TeamCityCacheUpdater', TeamCityCacheUpdater