
class CacheService
    constructor: ($timeout) ->
        @Store = {}
        @_updaters = {}
        @Restore()
        @timeout = $timeout

    later: (fn) -> @timeout fn, 100

    Restore: ->
        chrome.runtime.sendMessage (type: 'CACHE_GET'), (result) =>
            if result?.isSuccess and result?.store
                @Store = result?.store

    OrderUpdate: (key, args) ->
        @later => @Update key, await @_updaters[key]?(args)

    Update: (key, data) ->
        @Store[key] = data
        @Save()

    RegisterUpdater: (key, fn) ->
        @_updaters[key] = fn

    UnregisterUpdater: (key) ->
        delete @_updaters[key]

    Save: ->
        chrome.runtime.sendMessage type: 'CACHE_SET', data: @Store, (result) ->
            unless result?.isSuccess
                Materialize.toast '
                    <div>
                        <span style="color:#f44336">Ouch... Error while caching</span>
                        <br/>
                        <small>Please, check console for details</small>
                    </div>
                ', 5000
                if result?.error then console.error result.error

angular.module('cache')
.service 'CacheService', CacheService