angular.module 'options', [
    'ngAnimate'
    'angularMaterializeAutoComplete'
    
    'utils'
    'cache'
]
.run (TfsCacheUpdater, TeamCityCacheUpdater) ->