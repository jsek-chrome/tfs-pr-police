microsecondsPerWeek = 1000 * 60 * 60 * 24 * 7

angular.module('utils')
.factory 'history', ->
    MostPopularLast7Days: ({ match }) ->
        new Promise (resolve, reject) ->
            oneWeekAgo = (new Date).getTime() - microsecondsPerWeek
            chrome.history.search {
                text: match,
                startTime: oneWeekAgo
            }, (items) ->
                items.sort (a,b) -> a.visitCount < b.visitCount ? 1 : -1
                resolve items[0]