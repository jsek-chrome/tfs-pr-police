jsonFetchOptions = 
    credentials: 'include'
    headers:
        'Access-Control-Allow-Origin': '*'
        'Accept' : 'application/json'

angular.module('utils')
.factory 'fetch', ->
    json: (url) ->
        response = await fetch url, jsonFetchOptions
        await response.json()

    check: (url) ->
        response = await fetch url, jsonFetchOptions
        return response.status is 200
