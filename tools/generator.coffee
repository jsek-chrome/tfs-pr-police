gulp = require 'gulp'
$ = do require 'gulp-load-plugins'
parallel = (x) -> gulp.parallel.apply gulp, x
{basename} = require 'path'

module.exports = (context, libs, scripts) ->
    gulp.task "build:#{context}:scripts", ->
        gulp.src scripts.map (x) -> "src/#{context}/scripts/#{x}"
            .pipe $.plumber()
            .pipe $.coffee2 bare: true
            .pipe $.concat 'scripts.js'
            .pipe $.size()
            .pipe gulp.dest "dist/#{context}"

    gulp.task "build:#{context}:styles", ->
        gulp.src "src/#{context}/styles/*.scss"
            .pipe $.plumber()
            .pipe $.postcss [
                require('precss') import: extension: 'scss'
            ]
            .pipe $.concat 'styles.css'
            .pipe $.size()
            .pipe gulp.dest "dist/#{context}"

    gulp.task "libs:#{context}:scripts", ->
        jsLibs = libs.filter (x) -> /\.js$/.test x
        return Promise.resolve() unless jsLibs.length > 0
        gulp.src jsLibs
            .pipe $.concat 'libs.js'
            .pipe gulp.dest "dist/#{context}"

    gulp.task "libs:#{context}:styles", ->
        cssLibs = libs.filter (x) -> /\.css$/.test x
        return Promise.resolve() unless cssLibs.length > 0
        gulp.src cssLibs
            .pipe $.concat 'libs.css'
            .pipe gulp.dest "dist/#{context}"

    gulp.task "build:#{context}:templates", ->
        gulp.src "src/#{context}/**/*.jade"
            .pipe $.plumber()
            .pipe $.pug
                client: true
                compileDebug: false
            .pipe $.wrapFile
                wrapper: (content, file) ->
                    "(function (w){ #{content} w.TPL = w.TPL || {}; w.TPL['#{basename file.modName}'] = template; }(window));"
            .pipe $.concat 'templates.js'
            .pipe gulp.dest "dist/#{context}"

    gulp.task "build:#{context}:pages", ->
        gulp.src "src/#{context}/*.pug"
            .pipe $.plumber()
            .pipe $.pug()
            .pipe gulp.dest "dist/#{context}"

    gulp.task "build:#{context}", parallel [
        "build:#{context}:scripts"
        "build:#{context}:styles"
        "libs:#{context}:scripts"
        "libs:#{context}:styles"
        "build:#{context}:pages"
        "build:#{context}:templates"
    ]
