gulp = require 'gulp'
$ = do require 'gulp-load-plugins'
generate = require './tools/generator'

series = (x) -> gulp.series.apply gulp, x
parallel = (x) -> gulp.parallel.apply gulp, x

# #####################################
# Libraries and scripts for domains
#\

contexts = [
    'background'
    'content'
    'options'
    'popup'
].reduce ((o, next) => (o[next] = next) and o), {}

contextsTasks = (patternFn) ->
    Object.keys(contexts).map (k) -> patternFn k

generate contexts.background, [], [
    'background.coffee'
]

generate contexts.content, [
    'node_modules/jquery/dist/jquery.min.js'
    'node_modules/timeago/jquery.timeago.js'
], [
    'services/utils.coffee'
    'components/pull-request/pull-request.coffee'
    'components/mine-pull-requests/mine-pull-requests.coffee'
    'services/all-pull-requests.coffee'
    'services/web-page-data.coffee'
    'services/configuration-provider.coffee'
    'services/commit-message-formatter.coffee'
    'services/teamcity-connector.coffee'
    'services/teamcity-build-status.coffee'
    'services/pr-list-bootstrapper.coffee'
    'services/pr-details-bootstrapper.coffee'
    'content.coffee'
]

generate contexts.options, [
    'node_modules/jquery/dist/jquery.min.js'
    'node_modules/materialize-css/dist/js/materialize.min.js'
    'node_modules/materialize-css/dist/css/materialize.min.css'
    'node_modules/mdi/css/materialdesignicons.min.css'
    'node_modules/angular/angular.js'
    'node_modules/angular/angular-csp.css'
    'node_modules/angular-animate/angular-animate.min.js'
    'node_modules/angular-materializecss-autocomplete/angular-materializecss-autocomplete.js'
], [
    'utils.module.coffee'
    'utils/*.coffee'
    'cache.module.coffee'
    'cache/*.coffee'
    'options.module.coffee'
    'page/*.component.coffee'
]

generate contexts.popup, [
    'node_modules/jquery/dist/jquery.min.js'
    'node_modules/materialize-css/dist/js/materialize.min.js'
    'node_modules/materialize-css/dist/css/materialize.min.css'
    'node_modules/mdi/css/materialdesignicons.min.css'
], [
    'popup.coffee'
]

# #####################################
# Copy static assets
#

gulp.task 'copy:manifest', ->
    gulp.src 'src/manifest.json'
        .pipe gulp.dest 'dist'

gulp.task 'copy:images', ->
    gulp.src 'src/**/*.+(png|jpg|gif|svg)'
        .pipe gulp.dest 'dist'

gulp.task 'copy:fonts', ->
    gulp.src [
        'node_modules/materialize-css/dist/fonts/**/*.*'
        'node_modules/mdi/fonts/**/*.*'
    ]
        .pipe gulp.dest 'dist/fonts'

# #####################################
# Build tasks for watch
#

gulp.task 'build:scripts', parallel contextsTasks (c) -> "build:#{c}:scripts"
gulp.task 'build:styles', parallel contextsTasks (c) -> "build:#{c}:styles"
gulp.task 'build:pages', parallel contextsTasks (c) -> "build:#{c}:pages"
gulp.task 'build:templates', parallel contextsTasks (c) -> "build:#{c}:templates"

# #####################################
# Build everything
#

gulp.task 'build', parallel contextsTasks((c) -> "build:#{c}").concat [
    'copy:manifest'
    'copy:images'
    'copy:fonts'
]

# #####################################
# Clean everything
#

gulp.task 'clean', ->
    require('del') ['dist', 'bin']


# #####################################
# Create packages
#

gulp.task 'pack:zip', () ->
    {'7z' : _7z} = require('7zip')
    {spawn} = require('child_process')
    spawn(_7z, ['a', 'bin\\extension.zip', '.\\dist\\*'])

gulp.task 'pack:crx', (done) ->
    fs = require 'fs'
    {join} = require 'path'
    ChromeExtension = require 'crx'

    fs.mkdirSync('bin') unless fs.existsSync('bin')

    distPath = join __dirname, 'dist'
    crxPath = join __dirname, 'bin', 'extension.crx'
    privateKey = fs.readFileSync join __dirname, 'key.pem'
    crx = new ChromeExtension { privateKey } 
    await crx.load distPath
    crxBuffer = await crx.pack()

    fs.writeFile crxPath, crxBuffer, (err) ->
        console.log err if err
        done()

gulp.task 'pack', parallel [
    'pack:zip'
    'pack:crx'
]

gulp.task 'default', series [
    'clean'
    'build'
    'pack'
]

# #####################################
# Watch for changes (dev mode)
#

gulp.task 'watch', ->
    gulp.watch 'src/**/*.jade',     parallel ['build:templates']
    gulp.watch 'src/**/*.pug',      parallel ['build:pages']
    gulp.watch 'src/**/*.coffee',   parallel ['build:scripts']
    gulp.watch 'src/**/*.scss',     parallel ['build:styles']
    gulp.watch 'src/manifest.json', parallel ['copy:manifest']
