var repoName = 'NewConnect';
var buildType = 'bt4';
// var repoName = 'CapabilitiesProject';
// var buildType = 'CapabilitiesProject_1CommitGit';
var tfsAddress = 'http://tfs-emea.ihs.com:8080/tfs/emea_spa_collection/Castle';
var teamcityAddress = 'http://gda-build-05.globalintech.pl:8080';
var fetchOptions = {
        credentials: 'include',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Accept' : 'application/json'
        }
    };

var fetchJson = (url) => fetch(url, fetchOptions).then(x => x.json());

var getDetails = async (pr) => {
  var {sourceRefName, closedDate, completionOptions: { squashMerge } } = await fetchJson(`${tfsAddress}/_apis/git/repositories/${repoName}/pullRequests/${pr}`); 
  return {
      branch: sourceRefName.match(/refs\/heads\/(.+)/)[1],
      date: closedDate.slice(0,10),
      squashed: squashMerge || false
  }
}

var getStatus = async (branch) => {
  try {
    var {build, count} = await fetchJson(`${teamcityAddress}/guestAuth/app/rest/builds/?locator=buildType:${buildType},branch:(name:${branch})`); 
    return {
        status: build && build.length ? build[0].status : '<unknown>',
        count: count
    }
  } catch (err) {
    return { status: '<unknown>', count: -1 }
  }
}


Promise.all(
    $('.vc-pullrequest-results-row')
        .toArray()
        .map($)
        .map(x => { return {
            author: x.find('.vc-pullrequest-author-name').text(),
            target: x.find('.vc-pullrequest-entry-branch').attr('title'),
            pr: x.find('a.primary-text').attr('href').match(/pullrequest\/([0-9]+)/)[1]
        }})
        .filter(x => x.target == 'develop')
        .map(async (x) => {
            var { branch, date, squashed } = await getDetails(x.pr);
            var { status, count } = await getStatus(branch);
            return {
                pr: x.pr,
                author: x.author,
                branch: branch,
                target: x.target,
                date: date,
                status: status,
                buildCount: count,
                squashed: squashed
            };
        })
).then(array =>
    console.log('Author,Date,Source,Target,Id,Build Count,Status,Squashed\n' + 
        array
        .filter(x => /^feature/i.test(x.branch))
        .map(x => `${x.author.replace(', ', '')},${x.date},${x.branch},${x.target},${x.pr},${x.buildCount},${x.status},${x.squashed}`).join('\n'))
);